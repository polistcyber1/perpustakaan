<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Buku;

class BukuController extends Controller
{
    public function index(){
        $buku = Buku::all();
        return response()->json([
        'pesan' => 'berhasil',
        'buku' => $buku
        ], 200);
    }   

    public function create(request $request){
        $buku = new Buku;
        $buku->judul = $request->judul;
        $buku->deskripsi = $request->deskripsi;
        $buku->sampul = $request->sampul;
        $buku->save();

        return "Data berhasil masuk";
    }

    public function update(request $request, $id){
        $judul = $request->judul;
        $deskripsi = $request->deskripsi;
        $sampul = $request->sampul;

        $buku = Buku::find($id);
        $buku->judul = $judul;
        $buku->deskripsi = $deskripsi;
        $buku->sampul = $sampul;
        $buku->save();

        return "Data berhasil diupdate";
    }

    public function delete($id){
        $buku = Buku::find($id);
        $buku->delete();

        return "Data berhasil dihapus";
    }
}
